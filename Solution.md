# bookingserv

## Solution Overview

- swagger path `http://localhost:9090/swagger-ui.html`
- h2 console path `http://localhost:9090/h2-console` for credentials check `application.properties`
- crud workflow with failure case validation `BookingServiceTests.java`