package com.paypal.bfs.test.bookingserv.model.error;

public class ResourceNotFoundException extends RuntimeException{

    public ResourceNotFoundException(){
        super();
    }

    public ResourceNotFoundException(String errorMessage){
        super(errorMessage);
    }
}
