package com.paypal.bfs.test.bookingserv.model.error;

public class DuplicateResourceException extends RuntimeException{

    public DuplicateResourceException(){
        super();
    }

    public DuplicateResourceException(String errorMessage){
        super(errorMessage);
    }
}
