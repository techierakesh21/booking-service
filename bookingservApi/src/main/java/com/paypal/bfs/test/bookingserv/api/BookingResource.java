package com.paypal.bfs.test.bookingserv.api;

import com.paypal.bfs.test.bookingserv.model.BookingRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping(("/v1/bfs/booking"))
public interface BookingResource {

    @PostMapping
    ResponseEntity<BookingRequest> create(@Valid @RequestBody BookingRequest request);

    @GetMapping("/all")
    ResponseEntity<List<BookingRequest>> getAllBookings();

    @GetMapping("/{bookingId}")
    ResponseEntity<BookingRequest> getBooking(@PathVariable("bookingId") int bookingId);

    @PutMapping("/{bookingId}")
    ResponseEntity<BookingRequest> updateBooking(@PathVariable("bookingId") int bookingId, @Valid @RequestBody BookingRequest request);

    @DeleteMapping("/{bookingId}")
    ResponseEntity deleteBooking(@PathVariable("bookingId") int bookingId);

}
