package com.paypal.bfs.test.bookingserv.repository;

import com.paypal.bfs.test.bookingserv.model.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

}
