package com.paypal.bfs.test.bookingserv.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "addresses")
@Data
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private int id;

    private String line1;

    private String line2;

    private String state;

    private String city;

    @Column(name = "zip_code")
    private String zipCode;

}
