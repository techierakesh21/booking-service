package com.paypal.bfs.test.bookingserv.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id")
    private int id;

    @Column(name = "booking_external_id", unique = true)
    private String bookingExternalId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "check_in_date_time")
    private LocalDateTime CheckInDateTime;

    @Column(name = "check_out_date_time")
    private LocalDateTime CheckOutDateTime;

    @Column(name = "total_price")
    private int totalPrice;

    @Column(name = "deposit")
    private int deposit;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    private Address address;

}
