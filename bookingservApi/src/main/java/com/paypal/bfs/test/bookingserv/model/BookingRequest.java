package com.paypal.bfs.test.bookingserv.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookingRequest {

    @JsonProperty(value = "booking_id", access = JsonProperty.Access.READ_ONLY)
    private Integer bookingId;

    @NotBlank(message = "booking_external_id can not be empty")
    @JsonProperty(value = "booking_external_id")
    private String bookingExternalId;

    @NotBlank(message = "first_name can not be empty")
    @JsonProperty(value = "first_name", required = true)
    private String firstName;

    @NotBlank(message = "last_name can not be empty")
    @JsonProperty(value = "last_name", required = true)
    private String lastName;

    @NotNull(message = "date_of_birth can not be empty")
    @JsonProperty(value = "date_of_birth", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateOfBirth;

    @NotNull(message = "check_in_date_time can not be empty")
    @JsonProperty(value = "check_in_date_time", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "UTC")
    private ZonedDateTime CheckInDateTime;

    @NotNull(message = "check_out_date_time can not be empty")
    @JsonProperty(value = "check_out_date_time", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "UTC")
    private ZonedDateTime CheckOutDateTime;

    @NotNull(message = "total_price can not be empty")
    @JsonProperty(value = "total_price", required = true)
    private int totalPrice;

    @NotNull(message = "deposit can not be empty")
    @JsonProperty(value = "deposit", required = true)
    private int deposit;

    @JsonProperty(value = "address_id", access = JsonProperty.Access.READ_ONLY)
    private Integer addressId;

    @NotBlank(message = "line1 can not be empty")
    @JsonProperty(value = "line1", required = true)
    private String line1;

    @JsonProperty(value = "line2")
    private String line2;

    @NotBlank(message = "state can not be empty")
    @JsonProperty(value = "state", required = true)
    private String state;

    @NotBlank(message = "city can not be empty")
    @JsonProperty(value = "city", required = true)
    private String city;

    @NotBlank(message = "zip_code can not be empty")
    @JsonProperty(value = "zip_code", required = true)
    private String zipCode;

}
