package com.paypal.bfs.test.bookingserv.repository;

import com.paypal.bfs.test.bookingserv.model.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {

    Optional<Booking> findByBookingExternalId(String bookingExternalId);

}
