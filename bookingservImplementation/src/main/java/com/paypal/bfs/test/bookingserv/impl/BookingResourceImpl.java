package com.paypal.bfs.test.bookingserv.impl;

import com.paypal.bfs.test.bookingserv.api.BookingResource;
import com.paypal.bfs.test.bookingserv.model.entity.Booking;
import com.paypal.bfs.test.bookingserv.model.BookingRequest;
import com.paypal.bfs.test.bookingserv.model.error.DuplicateResourceException;
import com.paypal.bfs.test.bookingserv.model.error.ResourceNotFoundException;
import com.paypal.bfs.test.bookingserv.repository.BookingRepository;
import com.paypal.bfs.test.bookingserv.util.BookingRequestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class BookingResourceImpl implements BookingResource {

    @Autowired private BookingRepository bookingRepository;

    @Override
    public ResponseEntity<BookingRequest> create(BookingRequest request) {
        if(isExternIdExists(request.getBookingExternalId()))
            throw new DuplicateResourceException("Record already exists for bookingExternalId :" + request.getBookingExternalId());
        BookingRequest createdBooking = save(BookingRequestMapper.toEntity(request));
        return new ResponseEntity<>(createdBooking, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<BookingRequest>> getAllBookings() {
        List<Booking> bookings = bookingRepository.findAll();
        return new ResponseEntity<>(BookingRequestMapper.toResponse(bookings), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BookingRequest> getBooking(int bookingId) {
        Booking booking = getBookingById(bookingId);
        return new ResponseEntity<>(BookingRequestMapper.toResponse(booking), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<BookingRequest> updateBooking(int bookingId, BookingRequest request) {
        Booking bookingResponse = getBookingById(bookingId);
        BookingRequest updatedBooking = save(BookingRequestMapper.toEntity(request, bookingResponse));
        return new ResponseEntity<>(updatedBooking, HttpStatus.OK);
    }

    @Override
    public ResponseEntity deleteBooking(int bookingId) {
        getBookingById(bookingId);
        bookingRepository.deleteById(bookingId);
        return new ResponseEntity(HttpStatus.OK);
    }

    private boolean isExternIdExists(String bookingExternalId){
        Optional<Booking> response = bookingRepository.findByBookingExternalId(bookingExternalId);
        if(response.isPresent())
            return true;
        return false;
    }

    private BookingRequest save(Booking booking){
        Booking savedBooking = bookingRepository.save(booking);
        return BookingRequestMapper.toResponse(savedBooking);
    }

    private Booking getBookingById(int bookingId){
        Optional<Booking> bookingResponse = bookingRepository.findById(bookingId);
        if(!bookingResponse.isPresent()){
            throw new ResourceNotFoundException("No Record found for bookingId :" + bookingId);
        }
        return bookingResponse.get();
    }

}
