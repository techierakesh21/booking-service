package com.paypal.bfs.test.bookingserv.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class GenericUtils {

    public static Date parseDate(String dateString, String dateFormat){
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        try {
            return sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static LocalDateTime toIST(ZonedDateTime dateTime){
        return LocalDateTime.ofInstant(dateTime.toInstant(), ZoneId.of("Asia/Kolkata"));
    }

    public static LocalDateTime toUTC(ZonedDateTime dateTime){
        return LocalDateTime.ofInstant(dateTime.toInstant(), ZoneId.of("UTC"));
    }

    public static ZonedDateTime toUTCZonedDateTime(LocalDateTime dateTime){
        return ZonedDateTime.of(dateTime, ZoneId.of("UTC"));
    }

    public static ZonedDateTime toISTZonedDateTime(LocalDateTime dateTime){
        return ZonedDateTime.of(dateTime, ZoneId.of("Asia/Kolkata"));
    }

    public static ZonedDateTime parseZonedDateTime(String dateTimeString, String dateTimeFormat){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeFormat);
        ZonedDateTime dateTime = ZonedDateTime.parse(dateTimeString, dateTimeFormatter);
        return dateTime;
    }

}
