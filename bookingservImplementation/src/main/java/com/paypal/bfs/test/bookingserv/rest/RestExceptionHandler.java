package com.paypal.bfs.test.bookingserv.rest;

import com.paypal.bfs.test.bookingserv.model.error.DuplicateResourceException;
import com.paypal.bfs.test.bookingserv.model.error.ErrorResponse;
import com.paypal.bfs.test.bookingserv.model.error.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<ErrorResponse> handleConstraintViolation(ConstraintViolationException ex)
    {
        List<String> details = ex.getConstraintViolations()
                .parallelStream()
                .map(e -> e.getMessage())
                .collect(Collectors.toList());

        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST, details);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex)
    {
        List<String> details = ex.getBindingResult()
                .getFieldErrors()
                .parallelStream()
                .map(e -> e.getDefaultMessage())
                .collect(Collectors.toList());

        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST, details);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException ex)
    {
        List<String> details = Arrays.asList(ex.getMessage());
        ErrorResponse error = new ErrorResponse(HttpStatus.NO_CONTENT, details);
        return new ResponseEntity<>(error, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(DuplicateResourceException.class)
    public final ResponseEntity<ErrorResponse> handleDuplicateResourceException(DuplicateResourceException ex)
    {
        List<String> details = Arrays.asList(ex.getMessage());
        ErrorResponse error = new ErrorResponse(HttpStatus.CONFLICT, details);
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }

}
