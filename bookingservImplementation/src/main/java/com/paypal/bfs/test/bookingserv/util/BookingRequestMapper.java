package com.paypal.bfs.test.bookingserv.util;

import com.paypal.bfs.test.bookingserv.model.entity.Address;
import com.paypal.bfs.test.bookingserv.model.entity.Booking;
import com.paypal.bfs.test.bookingserv.model.BookingRequest;

import java.util.List;
import java.util.stream.Collectors;

public class BookingRequestMapper {

    public static Booking toEntity(BookingRequest request){
        if (request == null){
            return null;
        }
        Booking booking = new Booking();
        booking.setBookingExternalId(request.getBookingExternalId());
        booking.setFirstName(request.getFirstName());
        booking.setLastName(request.getLastName());
        booking.setDateOfBirth(request.getDateOfBirth());
        booking.setCheckInDateTime(GenericUtils.toUTC(request.getCheckInDateTime()));
        booking.setCheckOutDateTime(GenericUtils.toUTC(request.getCheckOutDateTime()));
        booking.setTotalPrice(request.getTotalPrice());
        booking.setDeposit(request.getDeposit());
        Address address = new Address();
        address.setLine1(request.getLine1());
        address.setLine2(request.getLine2());
        address.setState(request.getState());
        address.setCity(request.getCity());
        address.setZipCode(request.getZipCode());
        booking.setAddress(address);
        return booking;
    }

    public static List<Booking> toEntity(List<BookingRequest> requests){
        if (requests == null){
            return null;
        }
        return requests.stream()
                .map(e -> toEntity(e))
                .collect(Collectors.toList());
    }

    public static BookingRequest toResponse(Booking booking){
        if (booking == null){
            return null;
        }
        BookingRequest request = new BookingRequest();
        request.setBookingId(booking.getId());
        request.setBookingExternalId(booking.getBookingExternalId());
        request.setFirstName(booking.getFirstName());
        request.setLastName(booking.getLastName());
        request.setDateOfBirth(booking.getDateOfBirth());
        request.setCheckInDateTime(GenericUtils.toISTZonedDateTime(booking.getCheckInDateTime()));
        request.setCheckOutDateTime(GenericUtils.toISTZonedDateTime(booking.getCheckOutDateTime()));
        request.setTotalPrice(booking.getTotalPrice());
        request.setDeposit(booking.getDeposit());
        request.setAddressId(booking.getAddress().getId());
        request.setLine1(booking.getAddress().getLine1());
        request.setLine2(booking.getAddress().getLine2());
        request.setState(booking.getAddress().getState());
        request.setCity(booking.getAddress().getCity());
        request.setZipCode(booking.getAddress().getZipCode());
        return request;
    }

    public static List<BookingRequest> toResponse(List<Booking> bookings){
        if (bookings == null){
            return null;
        }
        return bookings.stream()
                .map(e -> toResponse(e))
                .collect(Collectors.toList());
    }

    public static Booking toEntity(BookingRequest request, Booking booking){
        if (request == null){
            return null;
        }
        booking.setBookingExternalId(request.getBookingExternalId());
        booking.setFirstName(request.getFirstName());
        booking.setLastName(request.getLastName());
        booking.setDateOfBirth(request.getDateOfBirth());
        booking.setCheckInDateTime(GenericUtils.toUTC(request.getCheckInDateTime()));
        booking.setCheckOutDateTime(GenericUtils.toUTC(request.getCheckOutDateTime()));
        booking.setTotalPrice(request.getTotalPrice());
        booking.setDeposit(request.getDeposit());
        Address address = new Address();
        address.setLine1(request.getLine1());
        address.setLine2(request.getLine2());
        address.setState(request.getState());
        address.setCity(request.getCity());
        address.setZipCode(request.getZipCode());
        booking.setAddress(address);
        return booking;
    }
}
