package com.paypal.bfs.test.bookingserv;

import com.paypal.bfs.test.bookingserv.api.BookingResource;
import com.paypal.bfs.test.bookingserv.model.BookingRequest;
import com.paypal.bfs.test.bookingserv.model.error.DuplicateResourceException;
import com.paypal.bfs.test.bookingserv.model.error.ResourceNotFoundException;
import com.paypal.bfs.test.bookingserv.util.GenericUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestPropertySource(value = "classpath:application.properties")
public class BookingServiceTests {

    @Autowired private BookingResource bookingResource;

    @Value("${booking.dateFormat}") private String dateFormat;

    private int bookingId1;
    private int bookingId2;
    private String bookingExternalId1;
    private String bookingExternalId2;
    private String checkInDate;
    private String checkOutDate;
    private String checkOutDate2;


    public BookingServiceTests(){
        bookingId1 = new Random().nextInt();
        bookingId2 = new Random().nextInt();
        bookingExternalId1 = UUID.randomUUID().toString();
        bookingExternalId2 = UUID.randomUUID().toString();
        checkInDate = "2016-08-22T14:30+05:30[Asia/Kolkata]";
        checkOutDate = "2016-08-23T14:30+05:30[Asia/Kolkata]";
        checkOutDate2 = "2016-08-23T18:30+05:30[Asia/Kolkata]";
    }

    /**
     * for testing the create operation
     */
    @Test
    @Order(1)
    public void testCreateBooking(){
        ResponseEntity<BookingRequest> response = bookingResource.create(buildCreateBookingRequestV1());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        bookingId1 = response.getBody().getBookingId();
    }

    /**
     * for testing the get operation
     */
    @Test
    @Order(2)
    public void testCreatedBooking(){
        ResponseEntity<BookingRequest> response = bookingResource.getBooking(bookingId1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    /**
     * for testing the get operation where bookingId is not present
     */
    @Test
    @Order(3)
    public void testGetBookingFailure(){
        assertThrows(ResourceNotFoundException.class, () -> {
            ResponseEntity<BookingRequest> response = bookingResource.getBooking(bookingId2);
        });
    }

    /**
     * for testing the create operation
     */
    @Test
    @Order(4)
    public void testCreateBookingV2(){
        ResponseEntity<BookingRequest> response = bookingResource.create(buildCreateBookingRequestV2());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        bookingId2 = response.getBody().getBookingId();
    }

    /**
     * for testing the get all operation
     */
    @Test
    @Order(5)
    public void testAllBooking(){
        ResponseEntity<List<BookingRequest>> response = bookingResource.getAllBookings();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        List<Integer> bookingIds = response.getBody().stream().map(BookingRequest::getBookingId).collect(Collectors.toList());
        assertEquals(true, bookingIds.contains(bookingId1));
        assertEquals(true, bookingIds.contains(bookingId2));
    }

    /**
     * for testing the update operation
     */
    @Test
    @Order(6)
    public void testUpdateBooking(){
        ResponseEntity<BookingRequest> response = bookingResource.updateBooking(bookingId1, buildUpdateBookingRequest());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(750, response.getBody().getTotalPrice());
    }

    /**
     * for testing the delete operation
     */
    @Test
    @Order(7)
    public void testDeleteBooking(){
        ResponseEntity response = bookingResource.deleteBooking(bookingId1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * for testing the delete operation failure case
     */
    @Test
    @Order(8)
    public void testDeleteBookingFailureCase(){
        assertThrows(ResourceNotFoundException.class, () -> {
            bookingResource.deleteBooking(bookingId1);
        });
    }

    /**
     * for testing the create operation failure where externalId exists
     */
    @Test
    @Order(9)
    public void testCreateBookingV2FailureCase(){
        assertThrows(DuplicateResourceException.class, () -> {
            ResponseEntity<BookingRequest> response = bookingResource.create(buildCreateBookingRequestV2());
        });
    }

    private BookingRequest buildCreateBookingRequestV1(){
        BookingRequest request = new BookingRequest();
        request.setBookingExternalId(bookingExternalId1);
        request.setFirstName("Test");
        request.setLastName("User");
        request.setDateOfBirth(GenericUtils.parseDate("1990-12-01", dateFormat));
        request.setCheckInDateTime(ZonedDateTime.parse(checkInDate));
        request.setCheckOutDateTime(ZonedDateTime.parse(checkOutDate));
        request.setTotalPrice(500);
        request.setDeposit(1500);
        request.setLine1("first lane");
        request.setState("KA");
        request.setCity("BLR");
        request.setZipCode("560038");
        return request;
    }

    private BookingRequest buildCreateBookingRequestV2(){
        BookingRequest request = new BookingRequest();
        request.setBookingExternalId(bookingExternalId2);
        request.setFirstName("Second");
        request.setLastName("User");
        request.setDateOfBirth(GenericUtils.parseDate("1990-12-01", dateFormat));
        request.setCheckInDateTime(ZonedDateTime.parse(checkInDate));
        request.setCheckOutDateTime(ZonedDateTime.parse(checkOutDate));
        request.setTotalPrice(900);
        request.setDeposit(2400);
        request.setLine1("first lane");
        request.setState("KA");
        request.setCity("MYS");
        request.setZipCode("550789");
        return request;
    }

    private BookingRequest buildUpdateBookingRequest(){
        BookingRequest request = new BookingRequest();
        request.setBookingExternalId(bookingExternalId1);
        request.setFirstName("Test");
        request.setLastName("User");
        request.setDateOfBirth(GenericUtils.parseDate("1990-12-01", dateFormat));
        request.setCheckInDateTime(ZonedDateTime.parse(checkInDate));
        request.setCheckOutDateTime(ZonedDateTime.parse(checkOutDate));
        request.setTotalPrice(750);
        request.setDeposit(1500);
        request.setLine1("first lane");
        request.setLine2("Inner lane");
        request.setState("KA");
        request.setCity("BLR");
        request.setZipCode("560038");
        return request;
    }
}
